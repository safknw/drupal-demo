#!/usr/bin/env bash

set -ev

# Setup Drupal
$COMPOSER_BIN/blt setup --define drush.alias='${drush.aliases.ci}' --define environment=ci --no-interaction -v
# Web server start on pipeline
$COMPOSER_BIN/blt tests:server:start
# Run phpunit tests
$COMPOSER_BIN/phpunit -c $CI_PROJECT_DIR/tests/phpunit/ci-phpunit.xml $CI_PROJECT_DIR/web/modules/custom -v

set +v
