<?php

namespace Drupal\Tests\childcare_locator\Functional;

use Drupal\Tests\BrowserTestBase;

class CCLocatorSearchTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';
//  protected $profile = 'minimal';
  protected static $modules = ['childcare_locator'];

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
  }

  public function testDutchPostcodeValidation(){

    $this->drupalGet('childcare-finder');
    $this->assertSession()->statusCodeEquals(200);
    $edit = ['postcode' => 'a'];
    $this->submitForm($edit,t('Search'));
    $this->assertSession()->pageTextContains('Please enter correct postal code, ie 2312 NR.');
  }
}
