<?php
namespace Drupal\childcare_locator\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks that the postcode for dutch format.
 *
 * @Constraint(
 *   id = "DutchPostCode",
 *   label = @Translation("Podcast Duration", context = "Validation"),
 * )
 */
class DutchPostcodeConstraint extends Constraint {

  public $message = "Please enter correct Dutch postal code like 3056 LJ";
}
