<?php

namespace Drupal\childcare_locator\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DutchPostcodeConstraintValidator extends ConstraintValidator {

  public function validate($items, Constraint $constraint) {
    // This is a single-item field so we only need to
    // validate the first item
    $value = $items->first()->value;

    // Check that the value is in the format HH:MM:SS
    if ($value && !preg_match('/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i', $value)) {
      // The value is an incorrect format, so we set a 'violation'
      // aka error. The key we use for the constraint is the key
      // we set in the constraint, in this case $incorrectDurationFormat.
      $this->context->addViolation($constraint->message, ['%value' => $items]);
    }
  }

}
