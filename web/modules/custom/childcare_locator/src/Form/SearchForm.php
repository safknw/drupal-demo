<?php

namespace Drupal\childcare_locator\Form;

use Drupal\childcare_locator\Helper;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides a Childcare locator form.
 */
class SearchForm extends FormBase {


  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
  protected $entityTypeManager;

  /** @var \Drupal\Core\Database\Connection */
  protected Connection $db;

  /** @var \Drupal\childcare_locator\Helper */
  protected Helper $helper;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $db, Helper $helper) {
    $this->entityTypeManager = $entityTypeManager;
    $this->db = $db;
    $this->helper = $helper;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('childcare_locator.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'childcare_locator_search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['postcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#description' => $this->t('Enter valid Dutch postal code (3039 TG).'),
    ];

    $cc_type_options = [0 => 'All'];
//    $terms = $this->entityTypeManager->getStorage('taxonomy_term')
//      ->loadTree('childcare_category');
//    foreach ($terms as $term) {
//      $cc_type_options[$term->tid] = $term->name;
//    }
    $form['types'] = [
      '#type' => 'select',
      '#options' => $cc_type_options,
      '#title' => $this->t('Select childcare type'),
    ];

    $cities = [0 => 'All'];
//    $cities = array_merge($cities, $this->helper->getCitiesList());

    $form['city'] = [
      '#type' => 'select',
      '#options' => $cities,
      '#title' => $this->t('Select city'),
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Check if valid Dutch postal code is provided.
    if ($values['postcode'] && !preg_match('/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i', $values['postcode'])) {
      $form_state->setErrorByName('postcode', $this->t('Please enter correct postal code, ie 2312 NR.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    /** @var  \Drupal\Core\Entity\Query\QueryInterface $query */
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', 'childcare_facility')
      ->condition('status', 1);

    if ($values['postcode']) {
      $nearby_postcodes = $this->helper->getNearbyPostcodes($values['postcode']);
      $query->condition('field_postcode', $nearby_postcodes, 'in')
        ->range(0, 3);
    }
    // If value is not null.
    if ($values['city'] != '0') {
      $query->condition('field_city', $values['city']);
    }
    if ($values['types'] != '0') {
      $query->condition('field_childcare_category', ['target_id' => $values['types']]);
    }

    $nids = $query->execute();
    $items = [];
    if ($nids) {
      // Load nodes matching criteria.
      $nodes = Node::loadMultiple($nids);
      $view_builder = $this->entityTypeManager
        ->getViewBuilder(reset($nodes)->getEntityTypeId());
      // Get display for nodes.
      $items = $view_builder->viewMultiple($nodes, 'childcare_search');
    }

    $form['result_count'] = [
      '#markup' => $this->t('@count results found.', ['@count' => count($nids)]),
    ];
    $form['result'] = $items;

    $form_state->disableRedirect(TRUE);
  }

}
