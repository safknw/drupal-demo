<?php

namespace Drupal\childcare_locator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Childcare locator settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'childcare_locator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['childcare_locator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('childcare_locator.settings');
    $form['apikey'] = [
      '#type' => 'textfield',
      '#required' => 'required',
      '#title' => $this->t('API key'),
      '#description' => $this->t('API key for zipcodebase.com.'),
      '#default_value' => $config->get('apikey'),
    ];
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('Endpoint for zipcodebase.com.'),
      '#default_value' => $config->get('endpoint'),
    ];
    $form['radius'] = [
      '#type' => 'number',
      '#default_value' => $config->get('radius'),
      '#title' => $this->t('Radius'),
      '#description' => $this->t('Radius for searching nearby post codes.'),
    ];
    $form['unit'] = [
      '#type' => 'select',
      '#options' => [
        'km' => 'km',
        'miles' => 'miles',
      ],
      '#default_value' => $config->get('unit'),
      '#title' => $this->t('Unit for radius'),
    ];
    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('childcare_locator.settings')
      ->set('apikey', $form_state->getValue('apikey'))
      ->set('radius', $form_state->getValue('radius'))
      ->set('unit', $form_state->getValue('unit'))
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
