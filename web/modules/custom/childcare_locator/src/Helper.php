<?php

namespace Drupal\childcare_locator;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystem;
use GuzzleHttp\Client;

/**
 * Helper service.
 */
class Helper {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * The filesystem service.
   *
   * @var FileSystem
   */
  protected $fileSystem;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * Constructs a Helper object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Client $http_client,
                              Connection $connection,
                              FileSystem $fileSystem,
                              ConfigFactory $configFactory) {
    $this->httpClient = $http_client;
    $this->db = $connection;
    $this->fileSystem = $fileSystem;
    $this->configFactory = $configFactory;
  }

  /**
   * Get near by post codes.
   *
   * @param $postcode
   *
   * @return array
   */
  public function getNearbyPostcodes($postcode) {
    $nearby_postcodes = [$postcode];

    // Code for testing with locally saved results.
    /*
    $file = $this->fileSystem->realpath('public://radius-3056 LJ-2km.json');
    $data = file_get_contents($this->fileSystem->realpath($file));
    $json_data = Json::decode($data);
    $result = $json_data['results'];
    $codes = array_column($result, 'code');
    return array_merge($nearby_postcodes, $codes);
    */

    $config = $this->configFactory->get('childcare_locator.settings');
    $endpoint = $config->get('endpoint');
    $radius = $config->get('radius');
    $country = 'NL';
    $unit = $config->get('unit');
    $apikey = $config->get('apikey');

    // Url for fetching data.
    // https://app.zipcodebase.com/api/v1/radius?code=3056%20LJ&radius=5&country=NL&unit=km&apikey=<apikey>

    $params = [
      'code' => $postcode,
      'radius' => $radius,
      'unit' => $unit,
      'country' => $country,
      'apikey' => $apikey,
    ];
    $response = $this->httpClient->get($endpoint, [
      'query' => $params,
      'headers' => [
        'Accept' => 'Application/json',
      ],
    ]);
    if ($response->getStatusCode() === 200) {
      $data = $response->getBody();
      $json_data = Json::decode($data);
      if ($json_data['results']) {
        $result = $json_data['results'];
        $codes = array_column($result, 'code');
        $nearby_postcodes = array_merge($nearby_postcodes, $codes);
      }

    }
    return $nearby_postcodes;
  }

  /**
   * Get the list of distinct cities.
   *
   * @return array
   */
  public function getCitiesList(): array {
    $q = $this->db->select('node__field_city', 't')
      ->fields('t', ['field_city_value'])
      ->distinct()
      ->execute();

    $result = $q->fetchCol();
    if (!empty($result)) {
      return array_combine($result, $result);
    }
    else {
      return [];
    }
  }

}
