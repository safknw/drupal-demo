### Installation instruction.
Prerequisite - docker and lando (https://github.com/lando/lando).
* Clone the repo.
* Goto cloned repo.
* Run `composer install`.
* Run `lando start` to start local environment.
* To setup the site run `lando blt setup`.
* Goto `https://app.zipcodebase.com/`, and register and get free API key.
* Login as admin goto `/admin/config/services/childcare-locator` enter the API key.
* Add terms to `Childcare category` vocabulary.
* Create Childcare facility nodes.
* Goto `/childcare-finder` and search
